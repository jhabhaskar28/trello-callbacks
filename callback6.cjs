const path = require('path');
const fs = require('fs');
const callback1 = require(path.join(__dirname,"callback1.cjs"));
const callback2 = require(path.join(__dirname,"callback2.cjs"));
const callback3 = require(path.join(__dirname,"callback3.cjs"));

function callback6(fileName){

    setTimeout(() => {
        
        fs.readFile(path.join(__dirname,fileName),"utf-8",(err,data) => { 
        //Synchronously reads the file passed in the function argument 

            if(err){
                console.error("File read failed",err);
            } else {
                
                let boardArray = JSON.parse(data);
                let thanosBoardID = boardArray.filter((board) => {
                    return board.name.includes("Thanos");
                })
                .map((board) => {
                    return board.id;
                });

                callback1("boards.json",thanosBoardID[0],(err,data) => {
                //thanosBoardID[0] contains the ID for the 'Thanos' board

                    if(err){
                        console.error(err);
                    } else {
                        console.log(data);
                        
                        callback2("lists.json",thanosBoardID[0],(err,data) => {

                            if(err){
                                console.error(err);
                            } else {

                                console.log(data);
                                let fullListID = data.map((list) => {
                                    return list.id;
                                });
                                
                                let completedExecutions = 0;
                                let errorCount = 0;

                                for(let index = 0; index<fullListID.length; index ++){
                                    
                                    callback3("cards.json",fullListID[index],(err,data) => {

                                        if(err) {
                                            errorCount += 1;
                                            console.error(err);

                                            if(errorCount === fullListID.length) {
                                                console.log("No card fetched");
                                            }

                                        } else {
                                            completedExecutions += 1;
                                            console.log(data);

                                            if(completedExecutions === fullListID.length) {
                                                console.log("All cards fetched");
                                            }

                                        }

                                        if((completedExecutions + errorCount) === fullListID.length && errorCount > 0 && completedExecutions > 0) {
                                            console.log("Partial card data fetched");
                                        }

                                    });
                                }
                            }
                        });
                    }
                });
            }

        });

    }, 2*1000);
}

module.exports = callback6;