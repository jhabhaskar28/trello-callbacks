const path = require('path');

let callback1Path = path.join(__dirname,"../callback1.cjs");

let callback1 = require(callback1Path);

function callbackFunction(err,data){
    if(err){
        console.error(err);
    } else {
        console.log(data);
    }
}

callback1("boards.json","abc122dc",callbackFunction);