const path = require('path');

let callback2Path = path.join(__dirname,"../callback2.cjs");

let callback2 = require(callback2Path);

function callbackFunction(err,data){
    if(err){
        console.error(err);
    } else {
        console.log(data);
    }
}

callback2("lists.json","mcu453ed",callbackFunction);