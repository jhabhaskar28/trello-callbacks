const path = require('path');

let callback3Path = path.join(__dirname,"../callback3.cjs");

let callback3 = require(callback3Path);

function callbackFunction(err,data){
    if(err){
        console.error(err);
    } else {
        console.log(data);
    }
}

callback3("cards.json","ghnb768",callbackFunction);