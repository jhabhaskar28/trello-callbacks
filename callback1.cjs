const path = require('path');
const fs = require('fs');

function callback1(fileName, boardID, callbackFunction){

    setTimeout(() => {

        fs.readFile(path.join(__dirname,fileName),"utf-8",(err,data) => {//Synchronously reads the file passed in the function argument 
            if(err){
                console.log("Could not read file");

                callbackFunction(err);

            } else {
                console.log(`${fileName} read`);
                let boardArray = JSON.parse(data);
                let boardInformation = boardArray.filter((board) => {
                    return board.id === boardID;
                });

                callbackFunction(null,boardInformation);
            }
        });

    }, 2*1000);
}

module.exports = callback1;