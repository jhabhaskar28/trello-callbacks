const path = require('path');
const fs = require('fs');
const callback1 = require(path.join(__dirname,"callback1.cjs"));
const callback2 = require(path.join(__dirname,"callback2.cjs"));
const callback3 = require(path.join(__dirname,"callback3.cjs"));

function callback4(fileName){

    setTimeout(() => {
        
        fs.readFile(path.join(__dirname,fileName),"utf-8",(err,data) => { 
        //Synchronously reads the file passed in the function argument 

            if(err){
                console.error("File read failed",err);
            } else {
                
                let boardArray = JSON.parse(data);
                let thanosBoardID = boardArray.filter((board) => {
                    return board.name.includes("Thanos");
                })
                .map((board) => {
                    return board.id;
                });

                callback1("boards.json",thanosBoardID[0],(err,data) => {
                //thanosBoardID[0] contains the id for the 'Thanos' board
                    if(err){
                        console.error(err);
                    } else {
                        console.log(data);
                        
                        callback2("lists.json",thanosBoardID[0],(err,data) => {

                            if(err){
                                console.error(err);
                            } else {

                                console.log(data);
                                let mindListID = data.filter((list) => {
                                    return list.name.includes("Mind");
                                })
                                .map((list) => {
                                    return list.id;
                                });

                                callback3("cards.json",mindListID[0],(err,data) => {
                                //mindListID[0] contains the id for the 'Mind' list
                                    if(err) {
                                        console.error("callback3 call failed",err);
                                    } else {
                                        console.log(data);
                                    }
                                    
                                });
                            }
                        });
                    }
                });
            }

        });

    }, 2*1000);
}

module.exports = callback4;