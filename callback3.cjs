const path = require('path');
const fs = require('fs');

function callback3(fileName, listID, callbackFunction){

    setTimeout(() => {

        fs.readFile(path.join(__dirname,fileName),"utf-8",(err,data) => { //Synchronously reads the file passed in the function argument 
            if(err){
                console.log("Could not read file");

                callbackFunction(err);

            } else {
                console.log(`${fileName} read`);
                let cardsObject = JSON.parse(data);
                
                callbackFunction(null,cardsObject[listID]);
                
            }
        });

    }, 2*1000);
}

module.exports = callback3;