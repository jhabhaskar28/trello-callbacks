const path = require('path');
const fs = require('fs');

function callback2(fileName, boardID, callbackFunction){

    setTimeout(() => {

        fs.readFile(path.join(__dirname,fileName),"utf-8",(err,data) => { //Synchronously reads the file passed in the function argument 
            if(err){
                console.log("Could not read file");

                callbackFunction(err);

            } else {
                console.log(`${fileName} read`);
                let listsObject = JSON.parse(data);
                
                callbackFunction(null,listsObject[boardID]);
            }
        });

    }, 2*1000);
}

module.exports = callback2;